/*
  Реалізувати програму, яка циклічно показує різні картинки. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- У папці banners лежить HTML код та папка з картинками.
- При запуску програми на екрані має відображатись перша картинка.
- Через 3 секунди замість неї має бути показано друга картинка.
- Ще через 3 секунди – третя.
- Ще через 3 секунди – четверта.
- Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
- Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
- Після натискання на кнопку Припинити цикл завершується, на екрані залишається 
показаною та картинка, яка була там при натисканні кнопки.
- Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні 
якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.


Необов'язкове завдання підвищеної складності

- При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, 
що показує, скільки залишилося до показу наступної картинки.
- Робити приховування картинки та показ нової картинки поступовим 
(анімація fadeOut/fadeIn) протягом 0.5 секунди.
*/

"use strict";
const timerCycle = 3000;
let currentTime = timerCycle;
const tickTime = 10;
let timerId;

const startTimer = function () {
  timerId = setInterval(() => {
    currentTime -= tickTime;
    if (currentTime <= 0) {
      currentTime = timerCycle;
      changeImage();
    }
    showTime(currentTime);
  }, tickTime);
};
const stopTimer = function () {
  clearInterval(timerId);
};

const images = document
  .querySelector(".images-wrapper")
  .querySelectorAll(".image-to-show");
let imageIndex = 0;
let image = images[imageIndex];
const getNextImage = function () {
  if (imageIndex >= images.length - 1) imageIndex = 0;
  return images[++imageIndex];
};
const changeImage = function () {
  image.classList.remove("show");
  image = getNextImage();
  image.classList.add("show");
};
const timeContainer = document.querySelector("#time");
const showTime = function (time) {
  const sec = Math.floor(time / 1000);
  const msec = time - sec * 1000;
  timeContainer.innerText = `Left ${sec} sec ${msec} msec`;
};

const sliderControl = ({ target, currentTarget }) => {
  const button = target.closest("button");
  if (!button) return;
  currentTarget.querySelectorAll("button").forEach((button) => {
    button.disabled = "";
  });
  const action = button.dataset.action;
  switch (action) {
    case "start":
      startTimer();
      button.disabled = "disabled";
      break;
    case "stop":
      stopTimer();
      button.disabled = "disabled";
  }
};
const controls = document.querySelector(".controls");
controls.addEventListener("click", (event) => {
  sliderControl(event);
});
startTimer();
